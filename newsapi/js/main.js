window.onload = function() {
function handleSearchResults (searchTerm) {
  fetch(handleSearch(searchTerm)).then(function(response) {
    return response.json();
  }).then( function(data) {
    document.getElementById("container").innerHTML = '';
    handleRender(data.articles);
  });
}

function handleSearch (searchTerm) {

  var url = 'https://newsapi.org/v2/everything?' +
  'q=' + searchTerm + '&' +
  'apiKey=e4c05b0d5826435cb4af0f6e951a7de6';

  return new Request(url);
}

function handleRender (articles) {
  var container = document.getElementById("container");

  articles.forEach(function(article){
    var card = document.createElement("DIV");
    var cardImage = document.createElement("DIV");
    var img = document.createElement("img");
    var cardInfo = document.createElement("DIV");
    var a = document.createElement("a");
    var cardTitle = document.createElement("DIV");
    var cardAuthor = document.createElement("DIV");
    var cardDate = document.createElement("DIV");
    
    var img = document.createElement("img");


    card.className = "card";
    cardImage.className = "card__image-container";
    img.className = "card__image"
    cardInfo.className = "card__info";
    cardTitle.className = "card__info--title";
    cardAuthor.className = "card__info--author";
    cardDate.className = "card__info--date";
    

    img.setAttribute("src", article.urlToImage);
    a.setAttribute("href", article.url);
    a.setAttribute("target", "_blank");

    var cardTitleTextNode = document.createTextNode(article.title);
    var cardAuthorTextNode = document.createTextNode(article.author);
    var cardDateTextNode = document.createTextNode(article.publishedAt);
    

    container.appendChild(a);
    a.appendChild(card);
    card.appendChild(cardImage);
    cardImage.appendChild(img);
    card.appendChild(cardInfo);
    cardInfo.appendChild(cardTitle);
    cardTitle.appendChild(cardTitleTextNode);
    cardInfo.appendChild(cardAuthor);
    cardAuthor.appendChild(cardAuthorTextNode);
    cardInfo.appendChild(cardDate);
    cardDate.appendChild(cardDateTextNode);
    
  });
}

document.getElementById('search-form').addEventListener("submit", function(e){
  e.preventDefault();
  var searchTerm = e.target.elements.search.value;
  e.target.elements.search.value = '';
  handleSearchResults(searchTerm);
});



handleSearchResults('*');

};