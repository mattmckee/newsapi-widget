<?php
/*
Plugin Name: News API
Plugin URI: https://wordpress.org/plugins/health-check/
Description: Display news articles
Version: 1.0.0
Author: Matthew McKee
Author URI: http://health-check-team.example.com
*/

// Exit if accessed directly
if(!defined('ABSPATH')){
  exit;
}

// Load scripts
require_once(plugin_dir_path(__FILE__) . '/includes/newsapi-scripts.php');

// Load class
require_once(plugin_dir_path(__FILE__) . '/includes/newsapi-class.php');

//Register widget
function register_newsapi() {
  register_widget('News_API_Widget');
}

// Hook in function
add_action('widgets_init', 'register_newsapi');