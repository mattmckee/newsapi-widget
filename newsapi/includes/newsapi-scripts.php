<?php
  // Add Scripts
  function newsapi_add_scripts(){
    // Add Main Css
    wp_enqueue_style('newsapi-main-style', plugins_url() . '/newsapi/css/style.css');
    // Add Main JS
    wp_enqueue_script('newsapi-main-script', plugins_url() . '/newsapi/js/main.js');
  }

add_action('wp_enqueue_scripts', 'newsapi_add_scripts');